# -*- coding: utf-8 -*-
from services.chatwork import Chatwork
from services.db import Database
from services.endpoint import Endpoint
import requests
import json
from datetime import datetime, date, timedelta
import zipfile
import os
import urllib.request
import re
import csv
import glob
import shutil
import pprint
import boto3
import time
from urllib.parse import urlencode
import pytz

nowTz = datetime.now(pytz.UTC)
now = nowTz.strftime("%Y-%m-%d %H:%M:%S")
token = os.getenv('AWR_TOKEN')
tms = nowTz.strftime("%Y-%m-%d %H:%M:%S")

def kwGroupsDBModel(projectsData, kwGroups):
    dataModel = {
        "project_name": projectsData["name"],
        "project_id": projectsData["id"],
        "keyword_group_name": kwGroups["name"],
        "keyword_group_id": kwGroups["id"]
    }
    return dataModel


def insertAwrKwGroups(values):
    try:
        with Database() as db:
            columuns = [
                'created_at', 'updated_at', 'project_name', 'project_id', 'keyword_group_name', 'keyword_group_id', 'date'
            ]
            query = "INSERT INTO awr_keyword_groups ({}) VALUES %s".format(
                ','.join(columuns))
            vals = []
            for i in values:
                val = [
                    now, now, i['project_name'], i['project_id'], i['keyword_group_name'], i['keyword_group_id'], Endpoint().targetDate
                ]
                vals.append(val)

            db.execute_val(query, vals)
            db.commit
            db.close
        return 'ok'
    except Exception as err:
        print(err)
        msg = "[info][title]keywords insert error [/title]" + str(err) + "[/info]"
        Chatwork().sendChatwork(msg, os.getenv("ERROR_CW_ID"))

def selectDuplicate(projectId, keywordId) :
    with Database() as db:
        query = "SELECT * FROM awr_keyword_groups"
        query += " WHERE date = '" + Endpoint().targetDate + "'"
        query += " AND project_id = " + projectId
        query += " AND keyword_group_id = " + keywordId
        db.execute(query)
        res = db.fetchall()
        return res


def selectKeywordGroups() :
    with Database() as db:
        query = "SELECT * FROM awr_keyword_groups"
        query += " WHERE date = '" + Endpoint().targetDate + "'"
        query += " AND response_url is null"
        db.execute(query)
        res = db.fetchall()
        return res

def selectErrors() :
    with Database() as db:
        query = "SELECT * FROM awr_keyword_groups"
        query += " WHERE date = '" + Endpoint().targetDate + "'"
        query += " AND error_code is not null"
        db.execute(query)
        res = db.fetchall()
        return res


def updateExportUrls(updates) :
    try :
        res = None
        with Database() as db:
            updQuery = ""
            if updates["details"] != None :
                # responseUrl = urllib.parse.unquote(updates["details"], 'utf-8')
                updQuery += " ,response_url = '" + updates["details"] + "'"
            if updates["errorCode"] != None :
                updQuery += " ,error_code = '" + str(updates["errorCode"]) + "'"
            if updates["errorMessage"] != None :
                updQuery += " ,error_message = '" + updates["errorMessage"] + "'"

            if updQuery != "" :
                updQuery = "updated_at = '" + tms + "'" + updQuery
                query = "UPDATE awr_keyword_groups SET " + updQuery
                query+= " WHERE keyword_group_id = " + str(updates["KwGroupId"])
                query+= " AND project_id = " + str(updates["projectId"])
                query+= " AND date = '" + Endpoint().targetDate + "'"
                print(query)
                db.execute(query)
                db.commit
                db.close
                res = "ok"
        return res
    except Exception as err:
        print(err)
        msg = "[info][title]AWR visibility update error [/title]" + str(err) + "[/info]"
        Chatwork().sendChatwork(msg, os.getenv("ERROR_CW_ID"))

#************************************************************************************************************************************

if __name__ == "__main__":
    try :
        #現在登録されているプロジェクトを取得
        projects = Endpoint().getProjects()
        dataModels = []
        for projectsData in projects["projects"]:
            # if projectsData["id"] != "4": #あとで消す！
            #     continue
            #プロジェクトに登録されているキーワードを取得
            kwGroups = Endpoint().getKwGroups(projectsData["id"])
            for kwGroup in kwGroups:
                # All keywordsは無視
                if kwGroup["name"] == "All keywords":
                    continue

                dataCheck = selectDuplicate(projectsData["id"], kwGroup["id"])
                
                if len(dataCheck) == 0 : 
                    # print(dataCheck)
                    dataModel = kwGroupsDBModel(projectsData, kwGroup)
                    dataModels.append(dataModel)

        
        # インサート処理
        try : 
            if len(dataModels) > 0 :
                res = insertAwrKwGroups(dataModels)
                print(res)
        except Exception as err:
            print(err)
            msg = "[info][title]AWR keywords main function duplicate error [/title]" + str(err) + "[/info]"
            Chatwork().sendChatwork(msg, os.getenv("ERROR_CW_ID"))

#***************************************************************************************************************

        #今日分のKW GROUPをSELECT
        #3回まで繰り返す
        cnt = 0
        while cnt < 3 :
            cnt += 1
            kwGroups = selectKeywordGroups()
            if len(kwGroups) == 0 :
                break

            for kwGroup in kwGroups :
                #ZIPダウンロード用のリクエスト
                #9は何回やってもだめ
                if kwGroup["error_code"] == 9 :
                    continue
                res = Endpoint().execVisibilityExport(kwGroup["project_name"], kwGroup["keyword_group_name"], Endpoint().targetDate)
                res["KwGroupId"] = kwGroup["keyword_group_id"]
                res["projectId"] = kwGroup["project_id"]
                #URLまたはエラーが返ってくるのでアップデート
                if ("details" in res) == True : 
                    print(res["details"])
                upd = updateExportUrls(res)
                print(upd)

                #無駄遣いしそうなのでとりあえず抜ける
                if upd != "ok" :
                    break

        #エラーを通知する
        errors = selectErrors()
        if len(errors) > 0 :
            errNotifyMsg =  "[info][title]AWR ZIPダウンロードリクエストエラー[/title]"
            errNotifyMsg += "以下のエラーがありました"
            for errData in errors :
                errNotifyMsg += "\n[" + errData["project_name"] + "] " + errData["keyword_group_name"] + " / " + str(errData["error_code"]) + " : " + errData["error_message"] 
            errNotifyMsg += "[/info]"
            Chatwork().sendChatwork(errNotifyMsg, os.getenv("ERROR_CW_ID"))

    except Exception as err:
        print(err)
        msg = "[info][title]AWR keywords main function error [/title]" + str(err) + "[/info]"
        Chatwork().sendChatwork(msg, os.getenv("ERROR_CW_ID"))