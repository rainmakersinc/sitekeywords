from services.chatwork import Chatwork
from services.db import Database
import boto3
import os
from datetime import datetime, date, timedelta
import pytz
import io
import json
from smart_open import smart_open

#page_source_html の中身をtxtにしてs3にあげる(10件ずつくらい？)
#page_source_html の中身をnullにアップデート
#RDSの軽量化が目的

nowTz = datetime.now(pytz.UTC)
# now = nowTz.strftime("%Y-%m-%d %H:%M:%S")
tms = nowTz.strftime("%Y-%m-%d %H:%M:%S")

def selectPageSource() :
    with Database() as db:
        query = "SELECT id, page_source_html FROM page_source"
        query+= " WHERE page_source_html IS NOT NULL"
        # query += " AND id < 100000"
        query += " ORDER BY id"
        query += " LIMIT 1000"
        db.execute(query)
        res = db.fetchall()
        return res

def updateNullPageSource(id) :
    try :
        res = None
        with Database() as db:
            query = "UPDATE page_source SET"
            query+=" updated_at = '" + tms + "'" 
            query+= " ,page_source_html = null"
            query+= " WHERE id = " + str(id)
            
            db.execute(query)
            db.commit
            db.close
            res = "ok"
        return res
    except Exception as err:
        msg = "[info][title]page source update error [/title]" + str(err) + "[/info]"
        Chatwork().sendChatwork(msg, os.getenv("ERROR_CW_ID"))

if __name__ == "__main__":
    s3 = boto3.resource("s3").Bucket('ranking-tracker')

    #ファイル確認方法
    # for line in smart_open('s3://ranking-tracker/3.txt', 'rb'):
    #     print(line.decode('utf8'))    


    pageSources = selectPageSource()
    for i in pageSources :
        print(i["id"])
        # fileDir = "htmltxt/"
        fileDir = "/tmp/"
        fileName = str(i["id"])+'.txt'
        f = open(fileDir + fileName, 'w', encoding='UTF-8')
        f.write(i["page_source_html"])
        f.close()

        try :
            s3.upload_file(fileDir + fileName, fileName)
            upd = updateNullPageSource(i["id"])
        except Exception as err:
            msg = "[info][title]page source main error [/title]" + str(err) + "[/info]"
            Chatwork().sendChatwork(msg, os.getenv("ERROR_CW_ID"))   
