# from services.chatwork import Chatwork
# from services.db import Database
# from services.endpoint import Endpoint
# import requests
# import json
# from datetime import datetime, date, timedelta
# import zipfile
# import os
# import urllib.request
# from urllib.parse import urlencode
# import urllib.parse
# import re
# import csv
# import glob
# import shutil
# import pprint
# import boto3
# import time
# import pytz

# token = os.getenv('AWR_TOKEN')
# nowTz = datetime.now(pytz.UTC)
# tms = nowTz.strftime("%Y-%m-%d %H:%M:%S")


# def execVisibilityExport(project, kwGroup) :
#     res = {
#         "details" : None,
#         "errorCode" : None,
#         "errorMessage" : None
#     }
#     try :
#         payload = {
#             'action' : 'visibility_export',
#             'project' : project,
#             'token' : token,
#             'startDate' : Endpoint().startDate,
#             'stopDate' : Endpoint().stopDate,
#             'keywordgroup' : kwGroup
#         }
#         exportVisibilityRes = requests.get(Endpoint().urlGet, params=payload)
#         exportVisibilityRes.raise_for_status()
    
#         exportVisibilityJson = json.loads(exportVisibilityRes.text)
#         if exportVisibilityJson['response_code'] == 0 or exportVisibilityJson['response_code'] == 10 :
#             res["details"] = exportVisibilityJson["details"]
#         else :
#             res["errorCode"] = exportVisibilityJson["response_code"]
#             res["errorMessage"] = exportVisibilityJson["message"]
#             res["details"] = exportVisibilityJson["details"]

#     except requests.exceptions.RequestException as e:
#         errorRes = e.response.text
#         res["errorCode"] = errorRes["code"]
#         res["errorMessage"] = errorRes["message"]
    
#     finally:
#         return res



# def selectKeywordGroups() :
#     with Database() as db:
#         query = "SELECT * FROM awr_keyword_groups"
#         query += " WHERE date = '" + Endpoint().nowDate + "'"
#         query += " AND response_url is null"
#         db.execute(query)
#         res = db.fetchall()
#         return res


# def updateExportUrls(updates) :
#     try :
#         res = None
#         with Database() as db:
#             updQuery = ""
#             if updates["details"] != None :
#                 updQuery += " ,response_url = '" + urllib.parse.unquote(updates["details"], 'utf-8') + "'"
#             if updates["errorCode"] != None :
#                 updQuery += " ,response_url = '" + str(updates["errorCode"]) + "'"
#             if updates["errorMessage"] != None :
#                 updQuery += " ,response_url = '" + updates["errorMessage"] + "'"

#             if updQuery != "" :
#                 updQuery = "updated_at = '" + tms + "'" + updQuery
#                 query = "UPDATE awr_keyword_groups SET " + updQuery
#                 query+= " WHERE keyword_group_id = " + str(updates["KwGroupId"])
#                 query+= " AND project_id = " + str(updates["projectId"])
#                 query+= " AND date = '" + Endpoint().nowDate + "'"
            
#                 print(query)
#                 db.execute(query)
#                 db.commit
#                 db.close
#                 res = "ok"
#         return res
#     except Exception as err:
#         print(err)
#         msg = "[info][title]visibility update error [/title]" + str(err) + "[/info]"
#         Chatwork().sendChatwork(msg, os.getenv("ERROR_CW_ID"))


# if __name__ == "__main__":
#     try :
#         #今日分のKW GROUPをSELECT
#         kwGroups = selectKeywordGroups()
#         for kwGroup in kwGroups :
#             #ZIPダウンロード用のリクエスト
#             res = execVisibilityExport(kwGroup["project_name"], kwGroup["keyword_group_name"])
#             res["KwGroupId"] = kwGroup["keyword_group_id"]
#             res["projectId"] = kwGroup["project_id"]
#             #URLまたはエラーが返ってくるのでアップデート
#             upd = updateExportUrls(res)
#             print(upd)
#     except Exception as err:
#         print(err)
#         msg = "[info][title]visibility main function error [/title]" + str(err) + "[/info]"
#         Chatwork().sendChatwork(msg, os.getenv("ERROR_CW_ID"))