from services.chatwork import Chatwork
from services.db import Database
from services.endpoint import Endpoint
import requests
import json
from datetime import datetime, date, timedelta
import zipfile
import os
import urllib.request
import re
import csv
import glob
import shutil
import pprint
import boto3
import time
from urllib.parse import urlencode
import pytz

def selectProjectNames(targetDate) :
    with Database() as db:
        query = "SELECT distinct project_name FROM awr_keyword_groups"
        query += " WHERE date = '" + targetDate + "'"
        db.execute(query)
        res = db.fetchall()
        return res

if __name__ == "__main__":
    try :
        date = Endpoint().targetDate

        #現在登録されているプロジェクト名を取得
        projetcs = selectProjectNames(date)
        for project in projects :
            print(project["project_name"])

#         rankings = Endpoint().exportRanking()
#         dataModels = []
#         for projectsData in projects["projects"]:
#             # if projectsData["id"] != "4": #あとで消す！
#             #     continue
#             #プロジェクトに登録されているキーワードを取得
#             kwGroups = Endpoint().getKwGroups(projectsData["id"])
#             for kwGroup in kwGroups:
#                 # All keywordsは無視
#                 if kwGroup["name"] == "All keywords":
#                     continue

#                 dataCheck = selectDuplicate(projectsData["id"], kwGroup["id"])
                
#                 if len(dataCheck) == 0 : 
#                     # print(dataCheck)
#                     dataModel = kwGroupsDBModel(projectsData, kwGroup)
#                     dataModels.append(dataModel)

        
#         # インサート処理
#         try : 
#             if len(dataModels) > 0 :
#                 res = insertAwrKwGroups(dataModels)
#                 print(res)
#         except Exception as err:
#             print(err)
#             msg = "[info][title]keywords main function duplicate error [/title]" + str(err) + "[/info]"
#             Chatwork().sendChatwork(msg, os.getenv("ERROR_CW_ID"))

# #***************************************************************************************************************

#         #今日分のKW GROUPをSELECT
#         #3回まで繰り返す
#         cnt = 0
#         while cnt < 3 :
#             cnt += 1
#             kwGroups = selectKeywordGroups()
#             if len(kwGroups) == 0 :
#                 break

#             for kwGroup in kwGroups :
#                 #ZIPダウンロード用のリクエスト
#                 #9は何回やってもだめ
#                 if kwGroup["error_code"] == 9 :
#                     continue
#                 res = Endpoint().execVisibilityExport(kwGroup["project_name"], kwGroup["keyword_group_name"], Endpoint().targetDate)
#                 res["KwGroupId"] = kwGroup["keyword_group_id"]
#                 res["projectId"] = kwGroup["project_id"]
#                 #URLまたはエラーが返ってくるのでアップデート
#                 if ("details" in res) == True : 
#                     print(res["details"])
#                 upd = updateExportUrls(res)
#                 print(upd)

#                 #無駄遣いしそうなのでとりあえず抜ける
#                 if upd != "ok" :
#                     break

#         #エラーを通知する
#         errors = selectErrors()
#         if len(errors) > 0 :
#             errNotifyMsg =  "[info][title]ZIPダウンロードリクエストエラー[/title]"
#             errNotifyMsg += "以下のエラーがありました"
#             for errData in errors :
#                 errNotifyMsg += "\n[" + errData["project_name"] + "] " + errData["keyword_group_name"] + " / " + str(errData["error_code"]) + " : " + errData["error_message"] 
#             errNotifyMsg += "[/info]"
#             Chatwork().sendChatwork(errNotifyMsg, os.getenv("ERROR_CW_ID"))

    except Exception as err:
        print(err)
        msg = "[info][title]keywords main function error [/title]" + str(err) + "[/info]"
        Chatwork().sendChatwork(msg, os.getenv("ERROR_CW_ID"))