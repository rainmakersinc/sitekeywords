import os
from datetime import datetime, date, timedelta
import requests
import json
import urllib.request
from urllib.parse import urlencode


class Endpoint:
    def __init__(self):
        self.token = os.getenv('AWR_TOKEN')
        self.urlGet = 'https://api.awrcloud.com/v2/get.php'
        self.urlEndpoint = 'https://api.awrcloud.com/api/marketshare/v1/endpoint.php'
        self.now = datetime.now()
        self.targetDate = self.now.strftime('%Y-%m-%d')
        # self.targetDate = '2021-09-01'


    def execVisibilityExport(self, project, kwGroup, targetDate) :
        res = {
            "details" : None,
            "errorCode" : None,
            "errorMessage" : None
        }
        try :
            payload = {
                'action' : 'visibility_export',
                'project' : project,
                'token' : self.token,
                'startDate' : targetDate,
                'stopDate' : targetDate,
                'keywordgroup' : kwGroup
            }
            exportVisibilityRes = requests.get(self.urlGet, params=payload)
            exportVisibilityRes.raise_for_status()     
    
            exportVisibilityJson = json.loads(exportVisibilityRes.text)
            print(exportVisibilityJson['response_code'])
            print(exportVisibilityJson)

            #if exportVisibilityJson['response_code'] == 0 or exportVisibilityJson['response_code'] == 10 :
            if exportVisibilityJson['response_code'] == 0 :
                res["details"] = exportVisibilityJson["details"]
                #2重エンコード？
                res["details"] = urllib.parse.unquote(res["details"], 'utf-8')
                res["details"] = urllib.parse.unquote(res["details"], 'utf-8')
            elif exportVisibilityJson['response_code'] == 10 :
                res["details"] = exportVisibilityJson["details"]
                res["details"] = urllib.parse.unquote(res["details"], 'utf-8')
            else :
                res["errorCode"] = exportVisibilityJson["response_code"]
                res["errorMessage"] = exportVisibilityJson["message"]
                # res["details"] = exportVisibilityJson["details"]

        except requests.exceptions.RequestException as e:
            errorRes = e.response.text
            res["errorCode"] = errorRes["code"]
            res["errorMessage"] = errorRes["message"]
    
        finally:
            return res


    def getProjects(self):
        payload = {
            'action': "projects",
            'token': self.token
        }

        getUrl = '%s?%s' % (self.urlGet, urlencode(payload))
        getProjectsRes = requests.get(getUrl)
        if getProjectsRes.status_code == 200:
            jsonRes = json.loads(getProjectsRes.text)
            return jsonRes
        else:
            return getProjectsRes.status_code


    def getKwGroups(self, projectId):
        payload = {
            'action': "getKeywordGroups",
            'token': self.token,
            'projectId': projectId
        }

        getUrl = '%s?%s' % (self.urlEndpoint, urlencode(payload))
        getKwGroupsRes = requests.get(getUrl)
        if getKwGroupsRes.status_code == 200:
            jsonRes = json.loads(getKwGroupsRes.text)
            return jsonRes
        else:
            return getKwGroupsRes.status_code


    def exportRanking(self, projectName, targetDate):
        payload = {
            'action': "export_ranking",
            'token': self.token,
            'project': projectName,
            'startDate' : targetDate,
            'stopDate' : targetDate,
            'format' : 'json'
        }

        getUrl = '%s?%s' % (self.urlEndpoint, urlencode(payload))
        getKwGroupsRes = requests.get(getUrl)
        if getKwGroupsRes.status_code == 200:
            jsonRes = json.loads(getKwGroupsRes.text)
            return jsonRes
        else:
            return getKwGroupsRes.status_code

