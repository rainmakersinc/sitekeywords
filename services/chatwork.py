import requests
import pprint
import os


class Chatwork:
    def __init__(self):
        self.apiKey = os.getenv("CW_TOKEN")
        self.endpoint = 'https://api.chatwork.com/v2'

    def sendChatwork(self, msg, roomId):
        post_message_url = '{}/rooms/{}/messages'.format(self.endpoint, roomId)

        headers = { 'X-ChatWorkToken': self.apiKey}
        params = { 'body': msg}

        resp = requests.post(post_message_url,
                            headers=headers,
                            params=params)

        pprint.pprint(resp.content)