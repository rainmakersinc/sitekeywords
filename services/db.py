# coding: utf-8
import os
import psycopg2
from psycopg2.extras import execute_values
from dotenv import load_dotenv
load_dotenv()

class Database:
  def __init__(self):
    self._conn    = psycopg2.connect(
      database  = os.getenv('DB_NAME'),
      user      = os.getenv('DB_USER'),
      password  = os.getenv('DB_PASSWORD'),
      host      = os.getenv('DB_HOST'),
      port      = os.getenv('DB_PORT')
    )
    self._cursor  = self._conn.cursor()

  def __enter__(self):
    return self

  def __exit__(self, exc_type, exc_val, exc_tb):
    self.close()

  @property
  def connection(self):
    return self._conn

  @property
  def cursor(self):
    return self._cursor

  def commit(self):
    self.connection.commit()
  
  def close(self, commit=True):
    if commit:
      self.commit()
    self.connection.close()

  def execute(self, sql, params=None):
    self.cursor.execute(sql, params or ())

  def execute_val(self, sql, values):
    execute_values(self.cursor, sql, values)
  
  def fetchall(self):
    columns = [column[0] for column in self.cursor.description]
    return [dict(zip(columns, row)) for row in self.cursor.fetchall()]




