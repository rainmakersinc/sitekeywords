# -*- coding: utf-8 -*-
from services.chatwork import Chatwork
from services.db import Database
from services.endpoint import Endpoint
import requests
import json
from datetime import datetime, date, timedelta
import zipfile
import os
import urllib.request
from urllib.parse import urlencode
import urllib.parse
import re
import csv
import glob
import shutil
import pprint
import boto3
import time
import pytz
import io

tmp = '/tmp/'
archivepath = tmp + 'CSV'
s3 = boto3.resource("s3").Bucket('site-rank-check')
nowTz = datetime.now(pytz.UTC)
now = nowTz.strftime("%Y-%m-%d %H:%M:%S")
tms = nowTz.strftime("%Y-%m-%d %H:%M:%S")

def unzip(fileName, zipFile) :
    with zipfile.ZipFile(zipFile) as existing_zip:
        for info in existing_zip.infolist():
            #解凍 1つしかない想定だけど？
            existing_zip.extract(info, path=archivepath)
            bkPath = archivepath + '/' + fileName + '.csv'
            shutil.move(archivepath + '/' + info.filename, bkPath)
    return bkPath

def convertCsv(csvFile) :
    jsonList = []
    # CSV ファイルの読み込み
    next(csvFile)
    for row in csv.DictReader(csvFile):
        jsonList.append(row)
    return jsonList

def selectTodaysUrls(date) :
    with Database() as db:
        query = "SELECT * FROM awr_keyword_groups"
        query += " WHERE date = '" + date + "'"
        query += " AND response_url is not null"
        query += " AND is_success is not true"
        db.execute(query)
        res = db.fetchall()
        return res

def updateSuccessed(id) :
    try :
        res = None
        with Database() as db:
            query = "UPDATE awr_keyword_groups SET"
            query+=" updated_at = '" + tms + "'" 
            query+= " ,is_success = true"
            query+= " WHERE id = " + str(id)
            
            db.execute(query)
            db.commit
            db.close
            res = "ok"
        return res
    except Exception as err:
        msg = "[info][title]visibility update error [/title]" + str(err) + "[/info]"
        Chatwork().sendChatwork(msg, os.getenv("ERROR_CW_ID"))

def insertAwrVisibility(values):
    try:
        with Database() as db:
            columuns = [
                'created_at', 'updated_at', 'project_name', 'keyword_group_name', 'date' #4
                , 'website', 'visibility_percent', 'visibility_score', 'first_place', 'top_3', 'top_5', 'top_10', 'top_20', 'top_30' #9
                , 'ranked', 'not_ranked', 'average_rank', 'moved_up', 'moved_down', 'added', 'dropped', 'on_first_page', 'below_first_page' #9
                , 'net_gain', 'net_loss', 'click_share', 'estimated_visits', 'market_share', 'estimated_daily_traffic', 'awr_keyword_groups_id' #7
            ]
            query = "INSERT INTO awr_visibility ({}) VALUES %s".format(
                ','.join(columuns))
            vals = []
            for i in values["values"]:
                valObj = values["values"][i]
                val = [
                    now, now, values['project_name'], values['keyword_group_name'],  Endpoint().targetDate #4
                    , i, float(valObj["visibility_percent"].replace(',', '')), valObj["visibility_score"], valObj["first_place"], valObj["top_3"], valObj["top_5"], valObj["top_10"], valObj["top_20"], valObj["top_30"] #9
                    , valObj["ranked"], valObj["not_ranked"], float(valObj["average_rank"].replace(',', '')), valObj["moved_up"], valObj["moved_down"], valObj["added"], valObj["dropped"], valObj["on_first_page"], valObj["below_first_page"] #9
                    , valObj["net_gain"], valObj["net_loss"], float(valObj["click_share"].replace(',', '')), valObj["estimated_visits"], float(valObj["market_share"].replace(',', '')), valObj["estimated_daily_traffic"], values["awr_keyword_groups_id"]
                ]
                vals.append(val)

            db.execute_val(query, vals)
            db.commit
            db.close
        return 'ok'
    except Exception as err:
        msg = "[info][title]visibility insert error [/title]" + str(err) + "[/info]"
        Chatwork().sendChatwork(msg, os.getenv("ERROR_CW_ID"))


def visibilityDataModel(urlData, jsonList):
    dataModel = {
        "project_name": urlData["project_name"],
        "keyword_group_name": urlData["keyword_group_name"],
        "awr_keyword_groups_id" : urlData["id"],
        "values" : {}
    }
    metrikKey = ""
    for csvData in jsonList :
        for i in csvData :
            #一番にくること前提！
            if csvData[i] == "Visibility Score" :
                metrikKey = "visibility_score"
            elif csvData[i] == "Visibility Percent" :
                metrikKey = "visibility_percent"
            elif csvData[i] == "First Place" :
                metrikKey = "first_place"
            elif csvData[i] == "Top 3" :
                metrikKey = "top_3"
            elif csvData[i] == "Top 5" :
                metrikKey = "top_5"
            elif csvData[i] == "Top 10" :
                metrikKey = "top_10"
            elif csvData[i] == "Top 20" :
                metrikKey = "top_20"
            elif csvData[i] == "Top 30" :
                metrikKey = "top_30"
            elif csvData[i] == "Ranked" :
                metrikKey = "ranked"
            elif csvData[i] == "Not Ranked" :
                metrikKey = "not_ranked"
            elif csvData[i] == "Average Rank" :
                metrikKey = "average_rank"
            elif csvData[i] == "Moved Up" :
                metrikKey = "moved_up"
            elif csvData[i] == "Moved Down" :
                metrikKey = "moved_down"
            elif csvData[i] == "Added" :
                metrikKey = "added"
            elif csvData[i] == "First Place" :
                metrikKey = "first_place"
            elif csvData[i] == "Dropped" :
                metrikKey = "dropped"
            elif csvData[i] == "On First Page" :
                metrikKey = "on_first_page"
            elif csvData[i] == "Below First Page" :
                metrikKey = "below_first_page"
            elif csvData[i] == "Net Gain" :
                metrikKey = "net_gain"
            elif csvData[i] == "Net Loss" :
                metrikKey = "net_loss"
            elif csvData[i] == "Click Share" :
                metrikKey = "click_share"
            elif csvData[i] == "Estimated Visits" :
                metrikKey = "estimated_visits"
            elif csvData[i] == "Market Share" :
                metrikKey = "market_share"
            elif csvData[i] == "Estimated Daily Traffic" :
                metrikKey = "estimated_daily_traffic"
            else :
                metrikData = re.sub(r'\s\((.*)', '', csvData[i])
                if (i in dataModel["values"]) == False :
                    dataModel["values"][i] = {}
                dataModel["values"][i][metrikKey] = metrikData
    return dataModel


if __name__ == "__main__":
    try :
        date = Endpoint().targetDate
        urls = selectTodaysUrls(date)
        cnt = 0
        for urlData in urls :
            #3回連続失敗したら終了
            if cnt > 3 :
                msg = "[info][title]zip error [/title]ZIPがまだ作成されていないようです。時間を置いてやり直してください[/info]"
                Chatwork().sendChatwork(msg, os.getenv("ERROR_CW_ID"))
                exit()

            url = urlData["response_url"]
            getVisibilityRes = requests.get(url)
            if getVisibilityRes.status_code == 200 :
                fileName = None
                quoteUrl = urllib.parse.quote(url,safe='') 
                pattern = re.compile(r'(?<=fileName%3D)(.*)')
                if bool(pattern.search(quoteUrl)) == True :
                    fileName = pattern.search(quoteUrl).group(0)
                zipFileName = fileName + '.zip'
                # csvFileName = fileName + '.csv'

                #キーワードが日本語対応はめんどくさいので現在動いている方法でやります
                memFileName = 'visibility_api-' + urllib.parse.quote(urlData["project_name"], safe='') + '_' + date + '-' + date + '-' + urllib.parse.quote(urlData['keyword_group_name'], safe='')
                payload = {
                    'action' : 'get_visibility',
                    'project' : urlData["project_name"],
                    'token' : Endpoint().token,
                    'fileName' : memFileName
                }
                getUrl = '%s?%s' % (Endpoint().urlGet, urlencode(payload))
                print(getUrl)

                mem = urllib.request.urlopen(getUrl).read()

                #ファイルへの保存
                with open(zipFileName, mode="wb") as f:
                    f.write(mem)
                if zipfile.is_zipfile(zipFileName) == True :
                    csvFile = unzip(fileName, zipFileName)
                    shutil.move(zipFileName, tmp + 'BK')
                    csvDir = 'awr-rank-report/sitekw/' + str(date) +'/' +  fileName + '.csv'
                    s3.upload_file(csvFile, csvDir)
                      
                    #s3にアップしたらDBアップデート
                    res = updateSuccessed(urlData["id"])

                    #s3からcsvを取得する
                    csvData = io.TextIOWrapper(io.BytesIO(boto3.client('s3').get_object(Bucket="site-rank-check",Key=csvDir)['Body'].read()))
                    jsonList = convertCsv(csvData)
                    insertModel  = visibilityDataModel(urlData, jsonList)
                    res = insertAwrVisibility(insertModel)
                    print(res)    
                else :
                    cnt += 1     

        #件数確認
        checkUrls = selectTodaysUrls(date)
        if len(checkUrls) == 0 :
            msg = "[info][title]AWR visibility csv DB　インサート成功[/title]インサートが完了しました[/info]"
            Chatwork().sendChatwork(msg, os.getenv("ERROR_CW_ID"))
        else :
            msg = "[info][title]AWR visibility csv DB　インサート失敗[/title]"+ str(len(checkUrls)) + "件残っています。確認してください[/info]"
            Chatwork().sendChatwork(msg, os.getenv("ERROR_CW_ID"))
    except Exception as err:
        msg = "[info][title]AWR get visibility error [/title]" + str(err) + "[/info]"
        Chatwork().sendChatwork(msg, os.getenv("ERROR_CW_ID"))