from services.chatwork import Chatwork
from services.db import Database
from services.endpoint import Endpoint
import requests
import json
from datetime import datetime, date, timedelta
import zipfile
import os
import urllib.request
import re
import csv
import glob
import shutil
import pprint
import time
from urllib.parse import urlencode

def selectNotifyData() :
    with Database() as db:
        query = "SELECT am.project_name ,am.keyword_group_name , am.website"
        query+= ",am.notify_project_name ,am.notify_title ,am.goal ,am.goal_count ,av.first_place , av.top_5"
        query+= " FROM awr_visibility av, awr_metrics am"
        query+= " WHERE am.project_name = av.project_name"
        query+= " AND am.keyword_group_name = av.keyword_group_name"
        query+= " AND am.website = av.website"
        query+= " AND av.date = '" + Endpoint().targetDate + "'"
        query+= " ORDER BY am.order"
        
        db.execute(query)
        res = db.fetchall()
        return res

if __name__ == "__main__":
    try :
        notifyData = selectNotifyData()
        msgBody = ""
        for nData in notifyData :
            keyMsg = nData["notify_project_name"] + '『' + nData["notify_title"] + '』目標：' + nData["goal"] + str(nData["goal_count"]) +  '個\n'
            firstPlace = str(nData["first_place"]).rjust(3)
            keyMsg+= '  １位獲得   ' +  firstPlace
            #目標未達成であれば追記
            if int(firstPlace) < nData['goal_count'] :
                notAchieved = nData['goal_count'] - int(firstPlace)
                keyMsg += '    (目標との差分%s個)' % (notAchieved)
            else :
                keyMsg+= '    🎉🎉🎉'
            keyMsg += '\n'
            top5 = str(nData["top_5"]).rjust(3)
            keyMsg += '  ５位圏内   ' + top5 + '\n'
            
            if msgBody != "" :
                msgBody = msgBody + '\n'
                            
            msgBody = msgBody + keyMsg


        if msgBody != "" :  
            msgBody = Endpoint().targetDate + '\n' + msgBody
            print(msgBody)
            msg = '[code]' + msgBody + '[/code]'
            Chatwork().sendChatwork(msg, os.getenv("NOTIFY_CW_ID"))
    except Exception as err:
        print(err)
        msg = "[info][title]report rank main function error [/title]" + str(err) + "[/info]"
        Chatwork().sendChatwork(msg, os.getenv("ERROR_CW_ID"))